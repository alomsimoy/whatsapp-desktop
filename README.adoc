= WhatsAppQT

Unofficial WhatsApp Web Desktop Client.

[IMPORTANT]
====
*Out of maintenance*

Personally, I haven't used WhatsApp since the last questionable change to the terms and conditions.
This means that I no longer support WhatsAppQT.

Anyone interested in the takeover is welcome to https://gitlab.com/bit3/whatsappqt/-/issues[get in touch].
====

image::images/screenshot-main.png[Main Window]

Attribution: Icon by Teguh Sulistio https://www.iconfinder.com/VISOEALE

== Flatpak config

The flatpak config file can be found in the link:https://github.com/flathub/io.bit3.WhatsAppQT/blob/master/io.bit3.WhatsAppQT.yml[flathub/io.bit3.WhatsAppQT] repository.

== Tweaks

=== Allow uploading files from everywhere in /home/user

Per default the app is only allowed to access the "downloads" folder, to make it possible to download files and upload from "downloads" folder.
That may be too restrictive for most. To grant access to your home directory, override filesystem access with:

----
flatpak override --filesystem=home io.bit3.WhatsAppQT
----

== Local development

=== Installing dependencies

It is recommend to use the *flatpak user mode* installation method.
In this mode the packages are installed into user space, without affecting the system.

==== flatpak user mode

[source,bash]
----
# add flathub remote
$ flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# installing required packages
$ flatpak install --user --assumeyes flathub org.kde.Platform//5.15 org.kde.Sdk//5.15 io.qt.qtwebengine.BaseApp//5.15
----

==== flatpak system mode

[source,bash]
----
# add flathub remote
$ sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# installing required packages
$ sudo flatpak install --assumeyes flathub org.kde.Platform//5.15 org.kde.Sdk//5.15 io.qt.qtwebengine.BaseApp//5.15
----

==== qt5 packages

For local code completion / assistance.
Not needed for compiling!

[source,bash]
----
$ sudo dnf install qt5-qtwebengine-devel
----

NOTE: In the Clion IDE it is necessary to define the `CMAKE_PREFIX_PATH` in the _CMake options_
      (_Settings - Build, Execution, Development - CMake_), e.g. `-DCMAKE_PREFIX_PATH=/usr/lib64/qt5` on fedora.
      (link:https://www.jetbrains.com/help/clion/qt-tutorial.html#configure-cmakelists[ref])

=== Building and running the application

[source,bash]
----
$ cd /path/to/whatsappqt
# build the app
$ flatpak-builder --force-clean build-dir io.bit3.WhatsAppQT.yml
# run the app
$ flatpak-builder --run build-dir io.bit3.WhatsAppQT.yml WhatsAppQT
----
